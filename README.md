# read_about_robots

You can gain more information about robots here, You have to read **"more_about_robots.pdf"** file to know about them.

- Create a new project as described in **"more_about_robots.pdf"** file
- Project name should starts with your full name followed by _robot

Good luck